1. buat Database

 create database myshop;

2. Membuat Table

create table users(
	id int auto_increment,
	name varchar(255),
	email varchar(255),
	password varchar(255),
	primary key(id)
);

create table categories(
	id int auto_increment,
	name varchar(255),
	primary key(id)
);

create table items(
	id int auto_increment,
	name varchar(255),
	description varchar(255),
	password varchar(255),
	category_id int,
	primary key(id),
	foreign key(categories_id) references categories(id)
);

3. Memasukan data pada table
	a. users
	insert into users(name, email, password) values ("Jhon Doe", "jhon@doe.com", "jhon123");
	insert into users(name, email, password) values ("Jane Doe", "jane@doe.com", "jenita123");

	b. categories
	insert into categories(name) values ("gadget");
	insert into categories(name) values ("cloth");
	insert into categories(name) values ("men");
	insert into categories(name) values ("women");
	insert into categories(name) values ("branded");

	c. items
	insert into items(name, description, price, stock, category_id) values ("Sumsang b50", "hape keren dari 	merk sumsang", 4000000, 100, 1);
	
	insert into items(name, description, price, stock, category_id) values ("uniklooh", "baju keren dari 	brand ternama", 50000, 50, 2);
	
	insert into items(name, description, price, stock, category_id) values ("IMHO Watch", "Jam tangan anak 	yang jujur banget", 2000000, 10, 1);

4. mengambil data dari database

	a. Mengambil data users
	select id, name, email FROM users;

	b. Mengambil data items
	select * from items WHERE price > 1000000;

	c. Menampilkan data items join dengan kategori
	select items.name, items.description, items.price, items.stock, items.category_id, categories.name FROM 	items INNER JOIN categories ON items.category_id = categories.id;

5. Mengubah Data dari Database
	UPDATE items SET price = 2500000 WHERE name = 'sumsang b50';
